# Scalability
# echo "Scala iswc"
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_1.json -o simulator/iswc/rpi23sb_1_c -c y
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_1.json -o simulator/iswc/rpi23sb_1_d -c n
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_2.json -o simulator/iswc/rpi23sb_2_c -c y
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_2.json -o simulator/iswc/rpi23sb_2_d -c n
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_3.json -o simulator/iswc/rpi23sb_3_c -c y
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_3.json -o simulator/iswc/rpi23sb_3_d -c n
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_4.json -o simulator/iswc/rpi23sb_4_c -c y
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_4.json -o simulator/iswc/rpi23sb_4_d -c n
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_5.json -o simulator/iswc/rpi23sb_5_c -c y
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_5.json -o simulator/iswc/rpi23sb_5_d -c n
#
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_d1.json -o simulator/iswc/rpi23sb_d1_c -c y
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_d1.json -o simulator/iswc/rpi23sb_d1_d -c n
#
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_d4.json -o simulator/iswc/rpi23sb_d4_c -c y
# python3 simulator/generator.py -t simulator/iswc/rpi23sb_d4.json -o simulator/iswc/rpi23sb_d4_d -c n

#############################

# echo "Distrib ISWC"
# python3 simulator/generator.py -t simulator/iswc/sb_d1.json -o simulator/iswc/sb_d1_d -c n
# python3 simulator/generator.py -t simulator/iswc/sb_d1.json -o simulator/iswc/sb_d1_c -c y
#
# python3 simulator/generator.py -t simulator/iswc/sb_d11.json -o simulator/iswc/sb_d11_d -c n
# python3 simulator/generator.py -t simulator/iswc/sb_d11.json -o simulator/iswc/sb_d11_c -c y
#
# python3 simulator/generator.py -t simulator/iswc/sb_d12.json -o simulator/iswc/sb_d12_d -c n
# python3 simulator/generator.py -t simulator/iswc/sb_d12.json -o simulator/iswc/sb_d12_c -c y
#
# python3 simulator/generator.py -t simulator/iswc/sb_d2.json -o simulator/iswc/sb_d2_d -c n
# python3 simulator/generator.py -t simulator/iswc/sb_d2.json -o simulator/iswc/sb_d2_c -c y
#
# python3 simulator/generator.py -t simulator/iswc/sb_d21.json -o simulator/iswc/sb_d21_d -c n
# python3 simulator/generator.py -t simulator/iswc/sb_d21.json -o simulator/iswc/sb_d21_c -c y
#
# python3 simulator/generator.py -t simulator/iswc/sb_d22.json -o simulator/iswc/sb_d22_d -c n
# python3 simulator/generator.py -t simulator/iswc/sb_d22.json -o simulator/iswc/sb_d22_c -c y
#
# python3 simulator/generator.py -t simulator/iswc/sb_d3.json -o simulator/iswc/sb_d3_d -c n
# python3 simulator/generator.py -t simulator/iswc/sb_d3.json -o simulator/iswc/sb_d3_c -c y
#
# python3 simulator/generator.py -t simulator/iswc/sb_d4.json -o simulator/iswc/sb_d4_d -c n
# python3 simulator/generator.py -t simulator/iswc/sb_d4.json -o simulator/iswc/sb_d4_c -c y
#
# python3 simulator/generator.py -t simulator/shapes/rpi3sb_s1.json -o simulator/shapes/rpi3sb_s1_d -c n
# python3 simulator/generator.py -t simulator/shapes/rpi3sb_s1.json -o simulator/shapes/rpi3sb_s1_c -c y

python3 simulator/generator.py -t simulator/ieee/minimal_local.json -o simulator/ieee/minimal_local_cdr -c cdr
python3 simulator/generator.py -t simulator/ieee/minimal_local.json -o simulator/ieee/minimal_local_cir -c cir
python3 simulator/generator.py -t simulator/ieee/minimal_local.json -o simulator/ieee/minimal_local_cdp -c cdp
python3 simulator/generator.py -t simulator/ieee/minimal_local.json -o simulator/ieee/minimal_local_cip -c cip
python3 simulator/generator.py -t simulator/ieee/minimal_local.json -o simulator/ieee/minimal_local_adp -c adp

# python3 simulator/generator.py -t simulator/coopis/mini_factory.json -o simulator/coopis/mini_factory_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/mini_factory.json -o simulator/coopis/mini_factory_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/mini_factory.json -o simulator/coopis/mini_factory_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/mini_factory.json -o simulator/coopis/mini_factory_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/mini_factory.json -o simulator/coopis/mini_factory_adp -c adp

# python3 simulator/generator.py -t simulator/coopis/default_factory.json -o simulator/coopis/default_factory_adp -c adp
# python3 simulator/generator.py -t simulator/coopis/small_factory.json -o simulator/coopis/small_factory_adp -c adp

#############################

# echo "Scala local"
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_0.json -o simulator/coopis/clone_f_0_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_0.json -o simulator/coopis/clone_f_0_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_0.json -o simulator/coopis/clone_f_0_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_0.json -o simulator/coopis/clone_f_0_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_0.json -o simulator/coopis/clone_f_0_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_1.json -o simulator/coopis/clone_f_1_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_1.json -o simulator/coopis/clone_f_1_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_1.json -o simulator/coopis/clone_f_1_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_1.json -o simulator/coopis/clone_f_1_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_1.json -o simulator/coopis/clone_f_1_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_2.json -o simulator/coopis/clone_f_2_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_2.json -o simulator/coopis/clone_f_2_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_2.json -o simulator/coopis/clone_f_2_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_2.json -o simulator/coopis/clone_f_2_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/scala_syndream/clone_f_2.json -o simulator/coopis/clone_f_2_adp -c adp

#############################

echo "Scala distributed"
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/coopis/clone_f_0_rpi23sb_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/coopis/clone_f_0_rpi23sb_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/coopis/clone_f_0_rpi23sb_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/coopis/clone_f_0_rpi23sb_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/coopis/clone_f_0_rpi23sb_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/coopis/clone_f_1_rpi23sb_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/coopis/clone_f_1_rpi23sb_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/coopis/clone_f_1_rpi23sb_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/coopis/clone_f_1_rpi23sb_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/coopis/clone_f_1_rpi23sb_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/coopis/clone_f_2_rpi23sb_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/coopis/clone_f_2_rpi23sb_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/coopis/clone_f_2_rpi23sb_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/coopis/clone_f_2_rpi23sb_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/coopis/clone_f_2_rpi23sb_adp -c adp
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/thesis/clone_f_0_rpi23sb_cdr -c cdr
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/thesis/clone_f_0_rpi23sb_cir -c cir
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/thesis/clone_f_0_rpi23sb_cdp -c cdp
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/thesis/clone_f_0_rpi23sb_cip -c cip
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_0_rpi23sb.json -o simulator/thesis/clone_f_0_rpi23sb_adp -c adp

python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/thesis/clone_f_1_rpi23sb_cdr -c cdr
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/thesis/clone_f_1_rpi23sb_cir -c cir
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/thesis/clone_f_1_rpi23sb_cdp -c cdp
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/thesis/clone_f_1_rpi23sb_cip -c cip
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_1_rpi23sb.json -o simulator/thesis/clone_f_1_rpi23sb_adp -c adp

python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/thesis/clone_f_2_rpi23sb_cdr -c cdr
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/thesis/clone_f_2_rpi23sb_cir -c cir
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/thesis/clone_f_2_rpi23sb_cdp -c cdp
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/thesis/clone_f_2_rpi23sb_cip -c cip
python3 simulator/generator.py -t simulator/thesis/scala_distributed/clone_f_2_rpi23sb.json -o simulator/thesis/clone_f_2_rpi23sb_adp -c adp


#############################
# echo "Specialisation"
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_baseline.json -o simulator/coopis/specialisation_baseline_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_baseline.json -o simulator/coopis/specialisation_baseline_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_baseline.json -o simulator/coopis/specialisation_baseline_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_baseline.json -o simulator/coopis/specialisation_baseline_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_baseline.json -o simulator/coopis/specialisation_baseline_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_implementation.json -o simulator/coopis/specialisation_implementation_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_implementation.json -o simulator/coopis/specialisation_implementation_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_implementation.json -o simulator/coopis/specialisation_implementation_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_implementation.json -o simulator/coopis/specialisation_implementation_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/specialisation/specialisation_implementation.json -o simulator/coopis/specialisation_implementation_adp -c adp

#############################
echo "Distribution local"
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_0.json -o simulator/thesis/distribution_0_cdr -c cdr
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_0.json -o simulator/thesis/distribution_0_cir -c cir
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_0.json -o simulator/thesis/distribution_0_cdp -c cdp
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_0.json -o simulator/thesis/distribution_0_cip -c cip
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_0.json -o simulator/thesis/distribution_0_adp -c adp

python3 simulator/generator.py -t simulator/thesis/distribution/distribution_1.json -o simulator/thesis/distribution_1_cdr -c cdr
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_1.json -o simulator/thesis/distribution_1_cir -c cir
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_1.json -o simulator/thesis/distribution_1_cdp -c cdp
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_1.json -o simulator/thesis/distribution_1_cip -c cip
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_1.json -o simulator/thesis/distribution_1_adp -c adp

python3 simulator/generator.py -t simulator/thesis/distribution/distribution_2.json -o simulator/thesis/distribution_2_cdr -c cdr
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_2.json -o simulator/thesis/distribution_2_cir -c cir
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_2.json -o simulator/thesis/distribution_2_cdp -c cdp
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_2.json -o simulator/thesis/distribution_2_cip -c cip
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_2.json -o simulator/thesis/distribution_2_adp -c adp

python3 simulator/generator.py -t simulator/thesis/distribution/distribution_3.json -o simulator/thesis/distribution_3_cdr -c cdr
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_3.json -o simulator/thesis/distribution_3_cir -c cir
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_3.json -o simulator/thesis/distribution_3_cdp -c cdp
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_3.json -o simulator/thesis/distribution_3_cip -c cip
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_3.json -o simulator/thesis/distribution_3_adp -c adp

python3 simulator/generator.py -t simulator/thesis/distribution/distribution_4.json -o simulator/thesis/distribution_4_cdr -c cdr
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_4.json -o simulator/thesis/distribution_4_cir -c cir
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_4.json -o simulator/thesis/distribution_4_cdp -c cdp
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_4.json -o simulator/thesis/distribution_4_cip -c cip
python3 simulator/generator.py -t simulator/thesis/distribution/distribution_4.json -o simulator/thesis/distribution_4_adp -c adp

# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_1.json -o simulator/coopis/distribution_1_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_1.json -o simulator/coopis/distribution_1_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_1.json -o simulator/coopis/distribution_1_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_1.json -o simulator/coopis/distribution_1_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_1.json -o simulator/coopis/distribution_1_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_2.json -o simulator/coopis/distribution_2_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_2.json -o simulator/coopis/distribution_2_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_2.json -o simulator/coopis/distribution_2_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_2.json -o simulator/coopis/distribution_2_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_2.json -o simulator/coopis/distribution_2_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_3.json -o simulator/coopis/distribution_3_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_3.json -o simulator/coopis/distribution_3_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_3.json -o simulator/coopis/distribution_3_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_3.json -o simulator/coopis/distribution_3_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/distribution_syndream/distribution_3.json -o simulator/coopis/distribution_3_adp -c adp

# echo "Distribution distrib"
# python3 simulator/generator.py -t simulator/iswc/sb_d1.json -o simulator/coopis/sb_d1_cdr -c cdr
# python3 simulator/generator.py -t simulator/iswc/sb_d1.json -o simulator/coopis/sb_d1_cir -c cir
# python3 simulator/generator.py -t simulator/iswc/sb_d1.json -o simulator/coopis/sb_d1_cdp -c cdp
# python3 simulator/generator.py -t simulator/iswc/sb_d1.json -o simulator/coopis/sb_d1_cip -c cip
# python3 simulator/generator.py -t simulator/iswc/sb_d1.json -o simulator/coopis/sb_d1_adp -c adp
#
# python3 simulator/generator.py -t simulator/iswc/sb_d2.json -o simulator/coopis/sb_d2_cdr -c cdr
# python3 simulator/generator.py -t simulator/iswc/sb_d2.json -o simulator/coopis/sb_d2_cir -c cir
# python3 simulator/generator.py -t simulator/iswc/sb_d2.json -o simulator/coopis/sb_d2_cdp -c cdp
# python3 simulator/generator.py -t simulator/iswc/sb_d2.json -o simulator/coopis/sb_d2_cip -c cip
# python3 simulator/generator.py -t simulator/iswc/sb_d2.json -o simulator/coopis/sb_d2_adp -c adp
#
# python3 simulator/generator.py -t simulator/iswc/sb_d3.json -o simulator/coopis/sb_d3_cdr -c cdr
# python3 simulator/generator.py -t simulator/iswc/sb_d3.json -o simulator/coopis/sb_d3_cir -c cir
# python3 simulator/generator.py -t simulator/iswc/sb_d3.json -o simulator/coopis/sb_d3_cdp -c cdp
# python3 simulator/generator.py -t simulator/iswc/sb_d3.json -o simulator/coopis/sb_d3_cip -c cip
# python3 simulator/generator.py -t simulator/iswc/sb_d3.json -o simulator/coopis/sb_d3_adp -c adp
#
# python3 simulator/generator.py -t simulator/iswc/sb_d4.json -o simulator/coopis/sb_d4_cdr -c cdr
# python3 simulator/generator.py -t simulator/iswc/sb_d4.json -o simulator/coopis/sb_d4_cir -c cir
# python3 simulator/generator.py -t simulator/iswc/sb_d4.json -o simulator/coopis/sb_d4_cdp -c cdp
# python3 simulator/generator.py -t simulator/iswc/sb_d4.json -o simulator/coopis/sb_d4_cip -c cip
# python3 simulator/generator.py -t simulator/iswc/sb_d4.json -o simulator/coopis/sb_d4_adp -c adp

# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_0.json -o simulator/coopis/distribution_rpi23sb_0_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_0.json -o simulator/coopis/distribution_rpi23sb_0_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_0.json -o simulator/coopis/distribution_rpi23sb_0_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_0.json -o simulator/coopis/distribution_rpi23sb_0_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_0.json -o simulator/coopis/distribution_rpi23sb_0_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_1.json -o simulator/coopis/distribution_rpi23sb_1_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_1.json -o simulator/coopis/distribution_rpi23sb_1_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_1.json -o simulator/coopis/distribution_rpi23sb_1_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_1.json -o simulator/coopis/distribution_rpi23sb_1_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_1.json -o simulator/coopis/distribution_rpi23sb_1_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_2.json -o simulator/coopis/distribution_rpi23sb_2_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_2.json -o simulator/coopis/distribution_rpi23sb_2_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_2.json -o simulator/coopis/distribution_rpi23sb_2_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_2.json -o simulator/coopis/distribution_rpi23sb_2_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_2.json -o simulator/coopis/distribution_rpi23sb_2_adp -c adp
#
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_3.json -o simulator/coopis/distribution_rpi23sb_3_cdr -c cdr
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_3.json -o simulator/coopis/distribution_rpi23sb_3_cir -c cir
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_3.json -o simulator/coopis/distribution_rpi23sb_3_cdp -c cdp
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_3.json -o simulator/coopis/distribution_rpi23sb_3_cip -c cip
# python3 simulator/generator.py -t simulator/coopis/distribution_distributed/distribution_3.json -o simulator/coopis/distribution_rpi23sb_3_adp -c adp

#########

# echo "ITL distribution"
# python3 simulator/generator.py -t simulator/itl/itl-topology-d1.json -o simulator/itl/itl_d1_cdr -c cdr
# python3 simulator/generator.py -t simulator/itl/itl-topology-d1.json -o simulator/itl/itl_d1_cir -c cir
# python3 simulator/generator.py -t simulator/itl/itl-topology-d1.json -o simulator/itl/itl_d1_cdp -c cdp
# python3 simulator/generator.py -t simulator/itl/itl-topology-d1.json -o simulator/itl/itl_d1_cip -c cip
# python3 simulator/generator.py -t simulator/itl/itl-topology-d1.json -o simulator/itl/itl_d1_adp -c adp
#
# python3 simulator/generator.py -t simulator/itl/itl-topology-d2.json -o simulator/itl/itl_d2_cdr -c cdr
# python3 simulator/generator.py -t simulator/itl/itl-topology-d2.json -o simulator/itl/itl_d2_cir -c cir
# python3 simulator/generator.py -t simulator/itl/itl-topology-d2.json -o simulator/itl/itl_d2_cdp -c cdp
# python3 simulator/generator.py -t simulator/itl/itl-topology-d2.json -o simulator/itl/itl_d2_cip -c cip
# python3 simulator/generator.py -t simulator/itl/itl-topology-d2.json -o simulator/itl/itl_d2_adp -c adp
#
# python3 simulator/generator.py -t simulator/itl/itl-topology-d3.json -o simulator/itl/itl_d3_cdr -c cdr
# python3 simulator/generator.py -t simulator/itl/itl-topology-d3.json -o simulator/itl/itl_d3_cir -c cir
# python3 simulator/generator.py -t simulator/itl/itl-topology-d3.json -o simulator/itl/itl_d3_cdp -c cdp
# python3 simulator/generator.py -t simulator/itl/itl-topology-d3.json -o simulator/itl/itl_d3_cip -c cip
# python3 simulator/generator.py -t simulator/itl/itl-topology-d3.json -o simulator/itl/itl_d3_adp -c adp
#
# python3 simulator/generator.py -t simulator/itl/itl-topology-d4.json -o simulator/itl/itl_d4_cdr -c cdr
# python3 simulator/generator.py -t simulator/itl/itl-topology-d4.json -o simulator/itl/itl_d4_cir -c cir
# python3 simulator/generator.py -t simulator/itl/itl-topology-d4.json -o simulator/itl/itl_d4_cdp -c cdp
# python3 simulator/generator.py -t simulator/itl/itl-topology-d4.json -o simulator/itl/itl_d4_cip -c cip
# python3 simulator/generator.py -t simulator/itl/itl-topology-d4.json -o simulator/itl/itl_d4_adp -c adp
