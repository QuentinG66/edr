package fr.irit.melodi.edr.in.tools;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;




import fr.irit.melodi.edr.errors.UnimplementedFeatureException;
import fr.irit.melodi.edr.node.Node;

public class Announce_tool {

public static String processAnnounce(String profile) throws UnimplementedFeatureException {
    Model m = ModelFactory.createDefaultModel();
    InputStream stream = new ByteArrayInputStream(profile.getBytes(StandardCharsets.UTF_8));
    m.read(stream, "http://example.org/ns#", "TTL");
    Node.getInstance().processUpstreamMessage(m);
    return Node.getInstance().getTopologyManager().extractNodeFromAnnounce(m);
}

}