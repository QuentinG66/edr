package fr.irit.melodi.edr.in.coap_resources;

// import org.apache.logging.log4j.LogManager;
// import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;

import org.eclipse.californium.core.CoapResource;

import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;
import java.io.IOException;
import java.io.StringWriter;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class CoapTopoInterestEndpoint extends CoapResource {

  //  private static final Logger LOGGER = LogManager.getLogger(CoapTopoInterestEndpoint.class);

    public CoapTopoInterestEndpoint(String name) {
        super(name);
    }

    @Override
	public void handleGET(CoapExchange exchange) {
        ObjectMapper om = new ObjectMapper();
		StringWriter sw = new StringWriter();
		try {
            om.writeValue(sw, Node.getInstance().getInterestManager().getNodeInterests());
            exchange.respond(VALID,sw.toString());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		exchange.respond(INTERNAL_SERVER_ERROR);
	}


   /* @GET
	@Path("/interests")
	public Response getInterests() {
		ObjectMapper om = new ObjectMapper();
		StringWriter sw = new StringWriter();
		try {
			om.writeValue(sw, Node.getInstance().getInterestManager().getNodeInterests());
			return Response.status(HttpURLConnection.HTTP_OK).entity(sw.toString()).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Response.status(500).build();
	}*/
    
 
	}


