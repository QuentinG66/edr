package fr.irit.melodi.edr.in.coap_resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

public class CoapDataRawEndpoint extends CoapResource {

    private static final Logger LOGGER = LogManager.getLogger(CoapDataRawEndpoint.class);

    public CoapDataRawEndpoint(String name) {
        super(name);
    }

    @Override
	public void handlePOST(CoapExchange exchange) {
		// accept the exchange and send an ack to the client
        exchange.accept();
        if (exchange.getRequestOptions().isContentFormat(MediaTypeRegistry.TEXT_CSV)) {
            String data = exchange.getRequestText();
            try {
                LOGGER.debug("[COAP] Received raw data : " + data);
                Node.getInstance().processNewRawData(data);
                exchange.respond(CREATED);
            } catch (Exception e) {
                e.printStackTrace();
                exchange.respond(INTERNAL_SERVER_ERROR);
            }
        }
        else {
            exchange.respond(UNSUPPORTED_CONTENT_FORMAT);
        }
	}


    /*
     * @POST
     * 
     * @Path("/raw")
     * 
     * @Consumes("text/csv") public Response pushRawData(String data) { try {
     * LOGGER.debug("Received raw data : " + data);
     * Node.getInstance().processNewRawData(data); return
     * Response.status(HttpURLConnection.HTTP_OK).build(); } catch (Exception e) {
     * e.printStackTrace(); return
     * Response.status(HttpURLConnection.HTTP_SERVER_ERROR).build(); }
     * 
     * }
     */

}
