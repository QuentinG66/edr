package fr.irit.melodi.edr.in.coap_resources;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import fr.irit.melodi.edr.node.Node;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class CoapDataEnrichEndpoint extends CoapResource {

    private static final Logger LOGGER = LogManager.getLogger(CoapDataEnrichEndpoint.class);

    public CoapDataEnrichEndpoint(String name) {
        super(name);
    }

    @Override
	public void handlePOST(CoapExchange exchange) {
		// accept the exchange and send an ack to the client
        exchange.accept();
        // 172 associated manually with format turtle (possible mistake)
        if (exchange.getRequestOptions().isContentFormat(172)) {
            String data = exchange.getRequestText();
            try {
                Model m = ModelFactory.createDefaultModel();
                InputStream stream = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
                // dois-je laisser une adresse HTTP ?
                m.read(stream, "http://example.com/ns#", "TTL");
                Node.getInstance().processNewData(m);
                LOGGER.trace("[COAP] Data enriched with turtle : " + data);
                exchange.respond(CREATED);
            } catch (Exception e) {
                e.printStackTrace();
                exchange.respond(INTERNAL_SERVER_ERROR);
            }
			
        }
        else {
            exchange.respond(UNSUPPORTED_CONTENT_FORMAT);
        }
	}

    /*
     * @POST
     * 
     * @Path("/enriched")
     * 
     * @Consumes("text/turtle") public Response pushEnrichedData(String data) { try
     * { Model m = ModelFactory.createDefaultModel(); InputStream stream = new
     * ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8)); m.read(stream,
     * "http://example.com/ns#", "TTL"); Node.getInstance().processNewData(m);
     * return Response.status(HttpURLConnection.HTTP_OK).build(); } catch (Exception
     * e) { e.printStackTrace(); return
     * Response.status(HttpURLConnection.HTTP_SERVER_ERROR).build(); }
     * 
     * }
     */
}