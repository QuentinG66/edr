package fr.irit.melodi.edr.in.resources;

import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.irit.melodi.edr.node.Node;

@Path("/rules")
public class RuleEndpoint {
	private static final Logger LOGGER = LogManager.getLogger(RuleEndpoint.class);

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRules() {
		LOGGER.trace("Retrieving rules");
		ObjectMapper mapper = new ObjectMapper();
		StringWriter sw = new StringWriter();
		try {
			mapper.writeValue(sw, Node.getInstance().getRuleManager().getAllRulesRef());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Response.status(HttpURLConnection.HTTP_OK).entity(sw.toString()).build();
	}

	@POST
	@Path("/add")
	@Consumes("text/turtle")
	public Response addRule(String rule) {
		try {
			LOGGER.trace("Received new rule " + rule);
			Node.getInstance().getRuleManager().addRule(rule);
			return Response.status(HttpURLConnection.HTTP_OK).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(HttpURLConnection.HTTP_SERVER_ERROR).build();
		}
	}

}
