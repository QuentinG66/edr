package fr.irit.melodi.edr.in.coap_resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

public class CoapRulesAddEndpoint extends CoapResource {

    private static final Logger LOGGER = LogManager.getLogger(CoapRulesAddEndpoint.class);

    public CoapRulesAddEndpoint(String name) {
		super(name);
	}

	@Override
	public void handlePOST(CoapExchange exchange) {
		// accept the exchange and send an ack to the client
		exchange.accept();
		// get the request payload
		String rule = exchange.getRequestText();
        try {
			LOGGER.trace("[COAP] Application - Received new rule " + rule);
			Node.getInstance().getRuleManager().addRule(rule);
			exchange.respond(CREATED);
		} catch (Exception e) {
            LOGGER.error("[COAP] Application - new rule not received, request text : " + rule);
			e.printStackTrace();
			exchange.respond(INTERNAL_SERVER_ERROR);
		}
	}

	}

