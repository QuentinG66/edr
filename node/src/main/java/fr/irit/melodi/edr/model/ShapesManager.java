package fr.irit.melodi.edr.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.in.resources.TopologyEndPoint;
import fr.irit.melodi.edr.node.Node;
import fr.irit.melodi.sparql.exceptions.NotAFolderException;
import fr.irit.melodi.sparql.files.FolderManager;
import fr.irit.tools.queries.QueryEngine;

public class ShapesManager {
	private static final Logger LOGGER = LogManager.getLogger(ShapesManager.class);
	
	private FolderManager queries;
	public ShapesManager(){
		try {
			this.queries = new FolderManager("queries/shapes");
			this.queries.loadQueries();
		} catch (NotAFolderException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isReportConform(Model report){
		String query = this.queries.getQueries().get("report_valid");
		return QueryEngine.askQuery(query, report);
	}
	
	public Map<String, List<String>> extractTransferNodes(Model inferences){
		Map<String, List<String>> transfers = new HashMap<String, List<String>>();
		for(Map<String, String> result : QueryEngine.selectQuery(
				this.queries.getQueries().get("get_transferable"), inferences)){
			if(!transfers.containsKey(result.get("rule"))) {
				transfers.put(result.get("rule"), new ArrayList<String>());
			}
			transfers.get(result.get("rule")).add(result.get("node"));
		}
		return transfers;
	}
	
	public List<String> extractProductions(Model inferences){
		List<String> productions = new ArrayList<>();
		for(Map<String, String> result : QueryEngine.selectQuery(this.queries.getQueries().get("get_productions"), inferences)){
			productions.add(result.get("property"));
		}
		return productions;
	}
	
	public boolean hasActivatedRule(Model inference){
		String query = this.queries.getQueries().get("has_active_rule");
		return QueryEngine.askQuery(query, inference);
	}
	
	public List<String> getDeductionsRule(Model inferences){
		List<String> rules = new ArrayList<>();
		for(Map<String, String> result : QueryEngine.selectQuery(
				this.queries.getQueries().get("get_deduction_rule"), inferences)){
			rules.add(result.get("rule"));
		}
		return rules;
	}
	
	/**
	 * Shapes related to the topology will be processed by the rule engine
	 * @see{deactivateNodeSensitiveElements}
	 */
	public void activateNodeSensitiveElements(){
		LOGGER.trace("Activating shapes");
		String query = this.queries.getQueries().get("activate_node_sensitive");
		Node.getInstance().getKBManager().updateModel(query);
	}
	
	/**
	 * Shapes related to the topology will no longer be processed by the rule engine
	 * @see{activateNodeSensitiveElements}
	 */
	public void deactivateNodeSensitiveElements(){
		LOGGER.trace("Deactivating shapes");
		String query = this.queries.getQueries().get("deactivate_node_sensitive");
		Node.getInstance().getKBManager().updateModel(query);
	}
}
