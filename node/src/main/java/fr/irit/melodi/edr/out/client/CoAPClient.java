
package fr.irit.melodi.edr.out.client;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;

//Client inutile pour l'instant
public class CoAPClient extends Thread {
    private int id;

    public CoAPClient(int num) {
        this.id = num;
    }

    public void run() {
        try {
            CoapClient client = new CoapClient("coap://localhost/");
            System.out.println("Client créé id : " + id + "\n");
            CoapResponse response = client.get();
            if (response != null) {

                System.out.println(response.getCode());
                System.out.println(response.getOptions());
                System.out.println(response.getResponseText());

            } else {

                System.out.println("Request failed");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
