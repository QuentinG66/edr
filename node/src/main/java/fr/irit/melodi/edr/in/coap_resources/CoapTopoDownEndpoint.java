package fr.irit.melodi.edr.in.coap_resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;
import fr.irit.melodi.edr.in.tools.Announce_tool;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

import java.io.StringWriter;
import org.apache.jena.rdf.model.Model;


public class CoapTopoDownEndpoint extends CoapResource {

    private static final Logger LOGGER = LogManager.getLogger(CoapTopoDownEndpoint.class);

    public CoapTopoDownEndpoint(String name) {
        super(name);
    }

    @Override
	public void handlePOST(CoapExchange exchange) {
	
        exchange.accept();

        LOGGER.debug("[COAP] " + Node.getInstance().getName() + " received remote announce from downstream");
        String profile = exchange.getRequestText();
		LOGGER.trace(profile);
       
            try {
                if (exchange.getRequestOptions().isContentFormat(MediaTypeRegistry.TEXT_PLAIN)) {
                    String remote = Announce_tool.processAnnounce(profile);
                    if (remote != null) {
                        LOGGER.debug("[COAP] (DOWN) Annouced node has uri " + remote);
                        Node.getInstance().addDownstreamNode(profile);
                        LOGGER.trace("[COAP] (DOWN) Announced content added to " + Node.getInstance().getName() + " KB");
                    }
                    Model announce = Node.getInstance().getKBManager().getAnnounceModel(false);
                    if (announce != null) {
                        LOGGER.debug("[COAP] (DOWN) Responding to annouce from " + remote);
                        StringWriter sw = new StringWriter();
                        announce.write(sw, "TTL");
                     //   LOGGER.debug("[COAP] (DOWN) Stringwriter content : " + sw.toString());
                        exchange.respond(CREATED, sw.toString());
                    } else {
                        exchange.respond(INTERNAL_SERVER_ERROR);
                    }
                } else {
                    exchange.respond(UNSUPPORTED_CONTENT_FORMAT);
                }
            
            } catch (Exception e) {
                e.printStackTrace();
                exchange.respond(BAD_REQUEST, "Malformed RDF");
            }
        }
	}


   /* @POST
	@Path("/announce/downstream")
	@Produces(MediaType.TEXT_PLAIN)
	public Response downstreamAnnounce(String profile) {
		LOGGER.debug(Node.getInstance().getName() + " received remote announce from downsream");
		LOGGER.trace(profile);
		try {
			String remote = processAnnounce(profile);
			if (remote != null) {
				LOGGER.debug("Annouced node has uri " + remote);
				// TODO : The downstream node gets two messages where one would only be
				// necessary
				// first, the update of the rules, and then the announce
				Node.getInstance().addDownstreamNode(profile);
				LOGGER.trace("Announced content added to " + Node.getInstance().getName() + " KB");
			}
			Model annouce = Node.getInstance().getKBManager().getAnnounceModel(false);
			if (annouce != null) {
				LOGGER.debug("Responding to annouce from " + remote);
				StringWriter sw = new StringWriter();
				annouce.write(sw, "TTL");
				return Response.status(HttpURLConnection.HTTP_OK).entity(sw.toString()).build();
			} else {
				return Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(HttpURLConnection.HTTP_BAD_REQUEST).entity("Malformed RDF").build();
		}
	}
    */

