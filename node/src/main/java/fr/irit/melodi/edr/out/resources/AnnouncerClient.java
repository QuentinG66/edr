package fr.irit.melodi.edr.out.resources;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import fr.irit.melodi.edr.node.Node;

public class AnnouncerClient {
	private static final Logger LOGGER = LogManager.getLogger(AnnouncerClient.class);

	public static String annouceToUpstream(String uri, String announce) {
		
		if (!(Node.getInstance().getConfiguration().getCoapmode())) { // HTTP Mode
			Client client = ClientBuilder.newClient(new ClientConfig());
			Boolean successfulConnection = false;
			while (!successfulConnection) {
				try {
					Response r = client.target(uri).path("/topology/announce/downstream").request()
							.post(Entity.entity(announce, MediaType.TEXT_PLAIN));
					successfulConnection = true;
					if (r.getStatus() == 200) {
						String remoteDescription = r.readEntity(String.class);
						LOGGER.debug("Upstream remote returned description " + remoteDescription);
						return remoteDescription;
					} else {
						LOGGER.error("Annouce failed with status code " + r.getStatus());
						return "";
					}
				} catch (Exception e) {
					LOGGER.warn("Connection to upper node failed");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}
		} else {
			// when Coapmode activated, a CoapServer is created
			String noHttpUri = uri.substring(4,uri.length());
			String uriCoap = "coap"+ noHttpUri +"/topology/announce/downstream";
			CoapClient client = new CoapClient(uriCoap);
			LOGGER.debug("[COAP] Announcer client target URI : " + uriCoap);
			LOGGER.debug("[COAP] Announce description : " + announce);
			Boolean successfulConnection = false;
			while (!successfulConnection) {
				try {
					// in first argument the payload, in second the content format
				CoapResponse response = client.post(announce, MediaTypeRegistry.TEXT_PLAIN);
				successfulConnection = true;
				if (response.isSuccess()) {
					String remoteDescription =response.getResponseText();
					LOGGER.debug("[COAP] Upstream remote returned description " + remoteDescription);
					return remoteDescription;
				} else {
					LOGGER.error("[COAP] Annouce failed with status code " + response.getCode());
					return "";
					}
				} catch (Exception e) {
					LOGGER.warn("[COAP] Connection to upper node failed");
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					
				}

			}
		}
		return null;
	}

	public static void annouceToDownstream(String uri, String announce) {
		if (!(Node.getInstance().getConfiguration().getCoapmode())) { // HTTP Mode
			Client client = ClientBuilder.newClient(new ClientConfig());
			Response r = client.target(uri).path("/topology/announce/upstream").request()
					.post(Entity.entity(announce, MediaType.TEXT_PLAIN));
			if (r.getStatus() == 200) {
				LOGGER.debug("Downstream remote notification ok from " + Node.getInstance().getName());
			} else {
				LOGGER.error("Downstream remote notification failed with status code " + r.getStatus() + " for "
						+ Node.getInstance().getName());
			}
		} else {
			CoapClient client = new CoapClient("coap://localhost/topology/announce/upstream");
			CoapResponse response = client.post(announce, MediaTypeRegistry.TEXT_PLAIN);
			if (response.isSuccess()) {
				LOGGER.debug("[COAP] Downstream remote notification ok from " + Node.getInstance().getName());
			} else {
				LOGGER.error("[COAP] Downstream remote notification failed with status code " + response.getCode() + " for "
				+ Node.getInstance().getName());
			}

		}
	}
}
