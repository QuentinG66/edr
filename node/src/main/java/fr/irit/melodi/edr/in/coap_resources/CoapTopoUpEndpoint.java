package fr.irit.melodi.edr.in.coap_resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.in.tools.Announce_tool;
import fr.irit.melodi.edr.node.Node;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

public class CoapTopoUpEndpoint extends CoapResource {

    private static final Logger LOGGER = LogManager.getLogger(CoapTopoUpEndpoint.class);

    public CoapTopoUpEndpoint(String name) {
        super(name);
    }

    @Override
	public void handlePOST(CoapExchange exchange) {
	
        exchange.accept();

        LOGGER.debug("[COAP] " + Node.getInstance().getName() + " received remote announce from upstream");
        String profile = exchange.getRequestText();
		LOGGER.trace(profile);
       
            try {
                if (exchange.getRequestOptions().isContentFormat(MediaTypeRegistry.TEXT_PLAIN)) {
                    String remote = Announce_tool.processAnnounce(profile);
                    if (remote != null) {
                        LOGGER.debug("[COAP] Annouced (UP) node has uri " + remote);
                        Node.getInstance().getTopologyManager().addUpstreamRemote(remote);
                    }
                    LOGGER.trace("[COAP] Announced (UP) content added to " + Node.getInstance().getName() + " KB");
                    exchange.respond(CREATED, Node.getInstance().getUri());
                } else {
                    exchange.respond(UNSUPPORTED_CONTENT_FORMAT);
                }
            
            } catch (Exception e) {
                e.printStackTrace();
                exchange.respond(BAD_REQUEST, "Malformed RDF");
            }
        }
	}


 /*   @POST
	@Path("/announce/upstream")
	@Produces(MediaType.TEXT_PLAIN)
	public Response upstreamAnnounce(String profile) {
		LOGGER.debug(Node.getInstance().getName() + " received remote announce from upstream");
		LOGGER.trace(profile);
		try {
			String remote = processAnnounce(profile);
			if (remote != null) {
				LOGGER.debug("Annouced node has uri " + remote);
				Node.getInstance().getTopologyManager().addUpstreamRemote(remote);
			}
			LOGGER.trace("Announced content added to " + Node.getInstance().getName() + " KB");
			return Response.status(HttpURLConnection.HTTP_OK).entity(Node.getInstance().getUri()).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(HttpURLConnection.HTTP_BAD_REQUEST).entity("Malformed RDF").build();
		}
	}*/
    