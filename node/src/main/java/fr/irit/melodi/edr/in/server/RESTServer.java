package fr.irit.melodi.edr.in.server;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import fr.irit.melodi.edr.in.resources.ApplicationEndpoint;
import fr.irit.melodi.edr.in.resources.DataEndpoint;
import fr.irit.melodi.edr.in.resources.ModelEndpoint;
import fr.irit.melodi.edr.in.resources.RuleEndpoint;
import fr.irit.melodi.edr.in.resources.TopologyEndPoint;

public class RESTServer extends Thread {

    private static final Logger LOGGER = LogManager.getLogger(RESTServer.class);
    private String name;
    private String baseUrl;
    private int port;
    private String url;
    private static HttpServer server;

    public RESTServer(String url, String name, int port){
        this.name = name;
        this.port = port;
        this.baseUrl = url;
        this.url = url +":"+port;
    }

    public int getListeningPort(){
        return this.port;
    }

    public String getURL(){
        return this.url;
    }

    public void run() {
        LOGGER.info("[H2TP_Node] Setting up REST server " + name);
        URI baseUri = UriBuilder.fromUri(this.baseUrl).port(this.port).build();
        LOGGER.info("Configuring resources");
        ResourceConfig rc = new ResourceConfig()
                .register(DataEndpoint.class)
                .register(RuleEndpoint.class)
                .register(TopologyEndPoint.class)
                .register(ModelEndpoint.class)
                .register(ApplicationEndpoint.class);
        LOGGER.info("Creating the HTTP server on adress "+baseUri+"...");
        server = GrizzlyHttpServerFactory.createHttpServer(baseUri, rc);
        try {
            LOGGER.info("Starting REST server");
            System.out.println("Starting REST server");
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
