package fr.irit.melodi.edr.in.coap_resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.errors.UnimplementedFeatureException;
import fr.irit.melodi.edr.node.Node;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

public class CoapAppRuleEndpoint extends CoapResource {

	private static final Logger LOGGER = LogManager.getLogger(CoapAppRuleEndpoint.class);

	public CoapAppRuleEndpoint(String name) {
		super(name);
	}

	@Override
	public void handlePOST(CoapExchange exchange) {
		// accept the exchange and send an ack to the client
		exchange.accept();
		// get the request payload
		String rule = exchange.getRequestText();
		LOGGER.trace("[COAP] Received new application rule " + rule);
		try {
			Node.getInstance().getRuleManager().addRule(rule);
		} catch (UnimplementedFeatureException e) {
			e.printStackTrace();
		}
		exchange.respond(CREATED);
	}

	/*
	 * @POST
	 * 
	 * @Path("/rule") public Response pushRule(String rule) throws
	 * UnimplementedFeatureException{
	 * LOGGER.trace("Received new application rule "+rule);
	 * Node.getInstance().getRuleManager().addRule(rule); return
	 * Response.status(HttpURLConnection.HTTP_OK).build(); }
	 */
}
