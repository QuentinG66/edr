package fr.irit.melodi.edr.in.coap_resources;

// import org.apache.logging.log4j.LogManager;
// import org.apache.logging.log4j.Logger;


import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

public class CoapTopoPingEndpoint extends CoapResource {

 //   private static final Logger LOGGER = LogManager.getLogger(CoapTopoPingEndpoint.class);

    public CoapTopoPingEndpoint(String name) {
        super(name);
    }

    @Override
	public void handleGET(CoapExchange exchange) {
            exchange.respond(VALID,"pong");
	}


  /*  @GET
	@Path("/ping")
	public Response ping() {
		return Response.status(HttpURLConnection.HTTP_OK).entity("pong").build();
	} */

}