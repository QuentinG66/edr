package fr.irit.melodi.edr.in.coap_resources;

import java.io.IOException;
import java.io.StringWriter;

import fr.irit.melodi.edr.node.Node;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CoapRulesAllEndpoint extends CoapResource {

     private static final Logger LOGGER = LogManager.getLogger(CoapModelAllEndpoint.class);

    public CoapRulesAllEndpoint(String name) {
        super(name);
    }

    @Override
	public void handleGET(CoapExchange exchange) {
        LOGGER.trace("[COAP° Application : Retrieving rules");
        ObjectMapper mapper = new ObjectMapper();
		StringWriter sw = new StringWriter();
		try {
			mapper.writeValue(sw, Node.getInstance().getRuleManager().getAllRulesRef());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		exchange.respond(VALID,sw.toString(),MediaTypeRegistry.APPLICATION_JSON);
	}


}
