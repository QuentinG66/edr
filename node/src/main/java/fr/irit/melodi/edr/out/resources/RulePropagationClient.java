package fr.irit.melodi.edr.out.resources;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.glassfish.jersey.client.ClientConfig;

import fr.irit.melodi.edr.node.Node;

public class RulePropagationClient {
	private static final Logger LOGGER = LogManager.getLogger(RulePropagationClient.class);

	public static void sendRule(String uri, String rule) {
		LOGGER.debug("Sending rule to endpoint " + uri);
		if (!(Node.getInstance().getConfiguration().getCoapmode())) {
			Client client = ClientBuilder.newClient(new ClientConfig());
			Response r = client.target(uri).path("/rules/add").request()// "application/rdf+ttl"
					.post(Entity.entity(rule, "text/turtle"));
			if (r.getStatus() == 200) {
				LOGGER.debug("Rule successfully transmitted");
			} else {
				LOGGER.error("Announce failed with status code " + r.getStatus());
			}
		} else {
			String noHttpUri = uri.substring(4,uri.length());
			String uriCoap = "coap"+ noHttpUri +"/rules/add";
			LOGGER.debug("[COAP] Sendind rule to endpoint" + uriCoap);
			CoapClient client = new CoapClient(uriCoap);
			// Mistake possible here, 172 => random value associated to format turtle (not defined in MediaTypeRegistry)
			// match with the post request associated
			CoapResponse response = client.post(rule, 172);
			if (response.isSuccess()){
				LOGGER.debug("[COAP] Rule successfully transmitted");
			} else {
				LOGGER.error("[COAP] Announce failed with status code " + response.getCode());
			}

		}
	}
}
