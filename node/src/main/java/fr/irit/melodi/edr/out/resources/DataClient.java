package fr.irit.melodi.edr.out.resources;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.glassfish.jersey.client.ClientConfig;

import fr.irit.melodi.edr.node.Node;

public class DataClient {
	private static final Logger LOGGER = LogManager.getLogger(DataClient.class);

	public static void sendData(String endpoint, String data) {
		if (!(Node.getInstance().getConfiguration().getCoapmode())) {
			Client client = ClientBuilder.newClient(new ClientConfig());
			LOGGER.debug("Sendind data to " + endpoint);
			Response r = client.target(endpoint).path("/data/enriched").request()
					.post(Entity.entity(data, "text/turtle"));
			if (r.getStatus() == 200) {
				LOGGER.debug("Data correclty transmitted");
			} else {
				LOGGER.error("Data transmission failed with status " + r.getStatusInfo());
			}
		} else {
			String noHttpUri = endpoint.substring(4,endpoint.length());
			String uriCoap = "coap"+ noHttpUri +"/data/enriched";
			LOGGER.debug("[COAP] Sendind data to " + uriCoap);
			CoapClient client = new CoapClient(uriCoap);
			// Mistake possible here, 172 => random value associated to format turtle (not defined in MediaTypeRegistry)
			// 172 match with the post handler associated
			CoapResponse response = client.post(data, 172);
			if (response.isSuccess()){
				LOGGER.debug("[COAP] Data correclty transmitted");
			} else {
				LOGGER.error("[COAP] Data transmission failed with code " +response.getCode());
			}
		}
	}
}
