package fr.irit.melodi.edr.out.resources;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import org.glassfish.jersey.client.ClientConfig;

import fr.irit.melodi.edr.node.Node;

public class SubscriptionClient {

	private static final Logger LOGGER = LogManager.getLogger(SubscriptionClient.class);

	public static void subscribeToSensor(String remoteUri, String listeningEndpoint) {
		if (!(Node.getInstance().getConfiguration().getCoapmode())) {
			Client client = ClientBuilder.newClient(new ClientConfig());
			Boolean successfulConnection = false;
			while (!successfulConnection) {
				try {
					Response r = client.target(remoteUri).path("/registration/listener").request()
							.post(Entity.entity(listeningEndpoint, MediaType.TEXT_PLAIN));
					successfulConnection = true;
					if (r.getStatus() == 200) {
						LOGGER.debug("Connected to sensor " + remoteUri);
					} else {
						LOGGER.error("Connection to sensor " + remoteUri + " failed : " + r.getStatusInfo());
					}
				} catch (Exception e) {
					LOGGER.warn("Connection to sensor " + remoteUri + " impossible, retrying.");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}
		} else {
			// when Coapmode activated, a CoapServer is created
			String noHttpUri = remoteUri.substring(4,remoteUri.length());
			String uriCoap = "coap"+ noHttpUri +"/registration/listener";
			String port = noHttpUri.substring(noHttpUri.length()-4,noHttpUri.length());
			CoapClient client = new CoapClient(uriCoap);
			Boolean successfulConnection = false;
			while (!successfulConnection) {
				try {
					// in first argument the payload, in second the content format
					LOGGER.debug("[COAP] Subscription client listeningEndpoint : " + listeningEndpoint);
					LOGGER.debug("[COAP] Port of sensor: " + port);
				CoapResponse response = client.post(port+listeningEndpoint, MediaTypeRegistry.TEXT_PLAIN);
				successfulConnection = true;
				if (response.isSuccess()) {
					LOGGER.debug("[COAP] Connected to sensor " + uriCoap);
				} else {
					LOGGER.error("[COAP] Connection to sensor  " + uriCoap +"  failed : " + response.getCode());
					}
				} catch (Exception e) {
					LOGGER.warn("[COAP] Connection to sensor    " + uriCoap + "    impossible, retrying.");
						try {
							e.printStackTrace();
							Thread.sleep(1000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					
				}

			}

		}
	}
}
