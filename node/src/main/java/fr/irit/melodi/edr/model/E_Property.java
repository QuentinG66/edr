package fr.irit.melodi.edr.model;

public enum E_Property {
	TEMPERATURE("https://w3id.org/laas-iot/adream#Temperature"),
	LUMINOSITY("https://w3id.org/laas-iot/adream#Luminosity"),
	TEMPERATURE_REQUEST("https://w3id.org/laas-iot/adream#TemperatureRequest"),
	POWER_CONSUMPTION("https://w3id.org/laas-iot/adream#PowerConsumption"),
	HUMIDITY("https://w3id.org/laas-iot/adream#Humidity"),
	NOISE("https://w3id.org/laas-iot/adream#Noise"),
	SMOKE("https://w3id.org/laas-iot/adream#Smoke"),
	PRESENCE("https://w3id.org/laas-iot/adream#Presence"),
	POWER_PRODUCTION("https://w3id.org/laas-iot/adream#PowerProduction"),
	OPENNESS("https://w3id.org/laas-iot/adream#Openness"),
	WIND_DIRECTION("https://w3id.org/laas-iot/adream#ADREAMWindDirection"),
	WIND_SPEED("https://w3id.org/laas-iot/adream#ADREAMWindSpeed"),
	ELECTRICAL_POWER("https://w3id.org/laas-iot/adream#ElectricalPower"),
	ROTATION("https://w3id.org/laas-iot/adream#RotationSpeed"),
	PLUVIOMETRY("https://w3id.org/laas-iot/adream#ADREAMPluviometry"),
	ELECTRICAL_TENSION("https://w3id.org/laas-iot/adream#ElectricalTension"),
	PYRANOMETRY("https://w3id.org/laas-iot/adream#ADREAMPyranometry"),
	HYGROMETRY("https://w3id.org/laas-iot/adream#ADREAMHygrometry"),
	ATHMOSPHERIC_PRESSURE("https://w3id.org/laas-iot/adream#ADREAMAthmosphericPressure"),
	MACHINE_STATE("https://w3id.org/laas-iot/adream#MachineState"),
	CONVEYOR_STATE("https://w3id.org/laas-iot/adream#ConveyorState"),
	SUPERVISOR_PRESENCE("https://w3id.org/laas-iot/adream#SupervisorPresence"),
	PARTICLE_LEVEL("https://w3id.org/laas-iot/adream#ParticleLevel"),
	PRODUCT_QUALITY("https://w3id.org/laas-iot/adream#ProductQuality"),
	CONVEYOR_SPEED("https://w3id.org/laas-iot/adream#ConveyorSpeed"),
	MACHINE_SPEED("https://w3id.org/laas-iot/adream#MachineSpeed"),
	POWER_REDISTRIBUTION("https://w3id.org/laas-iot/adream#PowerRedistribution");
	private String uri;
	private E_Property(String uri){
		this.uri = uri;
	}
	
	public String getUri(){
		return this.uri;
	}
	
	public static E_Property fromString(String value){
		if(value.equals("temperature")){
			return E_Property.TEMPERATURE;
		} else if(value.equals("luminosity")){
			return E_Property.LUMINOSITY;
		} else if(value.equals("temperature_request")){
			return E_Property.TEMPERATURE_REQUEST;
		} else if (value.equals("power_consumption")){
			return E_Property.POWER_CONSUMPTION;
		} else if (value.equals("humidity")){
			return E_Property.HUMIDITY;
		} else if(value.equals("noise")){
			return E_Property.NOISE;
		} else if(value.equals("smoke")){
			return E_Property.SMOKE;
		} else if(value.equals("presence")){
			return E_Property.PRESENCE;
		} else if(value.equals("power_redistribution")){
			return E_Property.POWER_REDISTRIBUTION;
		} else if(value.equals("power_production")){
			return E_Property.POWER_PRODUCTION;
		} else if(value.equals("openness")){
			return E_Property.OPENNESS;
		} else if(value.equals("wind_direction")){
			return E_Property.WIND_DIRECTION;
		} else if(value.equals("wind_speed")){
			return E_Property.WIND_SPEED;
		} else if(value.equals("power")){
			return E_Property.ELECTRICAL_POWER;
		} else if(value.equals("rotation")){
			return E_Property.ROTATION;
		} else if(value.equals("pluviometry")){
			return E_Property.PLUVIOMETRY;
		} else if(value.equals("electrical_tension")){
			return E_Property.ELECTRICAL_TENSION;
		} else if(value.equals("pyranometry")){
			return E_Property.PYRANOMETRY;
		} else if(value.equals("hygrometry")){
			return E_Property.HYGROMETRY;
		} else if(value.equals("athmospheric_pressure")){
			return E_Property.ATHMOSPHERIC_PRESSURE;
		} else if(value.equals("machine_state")){
			return E_Property.MACHINE_STATE;
		} else if(value.equals("conveyor_state")){
			return E_Property.CONVEYOR_STATE;
		} else if(value.equals("supervisor_presence")){
			return E_Property.SUPERVISOR_PRESENCE;
		} else if(value.equals("particle_level")){
			return E_Property.PARTICLE_LEVEL;
		} else if(value.equals("product_quality")){
			return E_Property.PRODUCT_QUALITY;
		} else if(value.equals("conveyor_speed")){
			return E_Property.CONVEYOR_SPEED;
		} else if(value.equals("machine_speed")){
			return E_Property.MACHINE_SPEED;
		} else {
			return null;
		}
	}
}
