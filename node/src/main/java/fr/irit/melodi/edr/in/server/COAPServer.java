package fr.irit.melodi.edr.in.server;

import org.eclipse.californium.core.CoapServer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.californium.core.CoapResource;
import fr.irit.melodi.edr.in.coap_resources.CoapAppRuleEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapDataEnrichEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapDataRawEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapModelLoadEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapRulesAddEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapRulesAllEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapTopoDownEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapTopoInterestEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapTopoPingEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapTopoPresenEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapTopoUpEndpoint;
import fr.irit.melodi.edr.in.coap_resources.CoapModelAllEndpoint;

public class COAPServer extends Thread {

    private static final Logger LOGGER = LogManager.getLogger(COAPServer.class);
    private String name;
    private String baseUrl;
    private int port;
    private String url;
    private static CoapServer server;

    public COAPServer(String url, String name, int port) {

        String noHttpUrl = url.substring(4,url.length());
		String urlCoap = "coap"+ noHttpUrl;
        this.name = name;
        this.port = port;
        this.baseUrl = urlCoap;
        this.url = url + ":" + port;

    }
    
    public int getListeningPort(){
        return this.port;
    }

    public String getURL(){
        return this.url;
    }

    public void run() {
        LOGGER.info("[COAP] Setting server Coap");
        server = new CoapServer(port);
        LOGGER.info("[COAP] Server Coap created on port : " + port);
        server.add(new CoapResource("app").add(new CoapAppRuleEndpoint("rule")),
                new CoapResource("data").add(new CoapDataEnrichEndpoint("enriched"), new CoapDataRawEndpoint("raw")),
                new CoapResource("model").add(new CoapModelLoadEndpoint("load"), new CoapModelAllEndpoint("all")),
                new CoapResource("rules").add(new CoapRulesAllEndpoint("all"), new CoapRulesAddEndpoint("add")),
                new CoapResource("topology").add(new CoapTopoPingEndpoint("ping"),
                        new CoapTopoPresenEndpoint("presentation"), new CoapTopoInterestEndpoint("interests"),
                        new CoapResource("announce").add(new CoapTopoDownEndpoint("downstream"),
                                new CoapTopoUpEndpoint("upstream"))));
        LOGGER.info("[COAP] Ressources configured on server Coap");
        server.start();
    }

}
