package fr.irit.melodi.edr.in.coap_resources;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

public class CoapTopoPresenEndpoint extends CoapResource {

    //private static final Logger LOGGER = LogManager.getLogger(CoapTopoPresenEndpoint.class);

    public CoapTopoPresenEndpoint(String name) {
        super(name);
    }

    @Override
	public void handleGET(CoapExchange exchange) {
            exchange.respond(VALID,Node.getInstance().getKBManager().getAnnouncement(true));
	}

  /*  @GET
	@Path("/presentation")
	public Response present() {
		return Response.status(HttpURLConnection.HTTP_OK)
				.entity(Node.getInstance().getKBManager().getAnnouncement(true)).build();
	}*/

}