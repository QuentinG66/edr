package fr.irit.melodi.edr.in.coap_resources;

// import org.apache.logging.log4j.LogManager;
// import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

public class CoapModelAllEndpoint extends CoapResource {

    //private static final Logger LOGGER = LogManager.getLogger(CoapModelAllEndpoint.class);

    public CoapModelAllEndpoint(String name) {
        super(name);
    }

    @Override
	public void handleGET(CoapExchange exchange) {
        String r_all = Node.getInstance().getKBManager().serializeModel();
		exchange.respond(VALID,r_all);
	}

    /*
     * @GET
     * 
     * @Path("/all") public Response getKB(){ return Response
     * .status(HttpURLConnection.HTTP_OK)
     * .entity(Node.getInstance().getKBManager().serializeModel()) .build(); }
     */
}
