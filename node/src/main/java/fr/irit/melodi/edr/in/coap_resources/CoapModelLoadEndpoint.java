package fr.irit.melodi.edr.in.coap_resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

public class CoapModelLoadEndpoint extends CoapResource {

    private static final Logger LOGGER = LogManager.getLogger(CoapModelLoadEndpoint.class);

    public CoapModelLoadEndpoint(String name) {
        super(name);
    }

    @Override
	public void handlePOST(CoapExchange exchange) {
		// accept the exchange and send an ack to the client
		exchange.accept();
		// get the request payload
		String uri = exchange.getRequestText();
        LOGGER.trace("[COAP] Received model to load : " + uri);
        Node.getInstance().getKBManager().loadModel(uri);
		exchange.respond(CREATED,"Model loaded");
	}

    /*
     * @POST
     * 
     * @Path("/load") public Response loadModel(String uri){
     * Node.getInstance().getKBManager().loadModel(uri); return
     * Response.status(HttpURLConnection.HTTP_OK) .entity("Model loaded").build(); }
     */

}
