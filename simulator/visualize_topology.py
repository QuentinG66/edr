import pandas as pd
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

def visualize(node_list, topo_name):
    topology = [[0 for x in node_list] for x in node_list]
    node_map = {}
    # Creation of a reverse DNS URL -> ID
    for n in node_list.values():
        node_map[n["uri"]+":"+str(n["port"])] = n["id"]

    # Creation of the communication matrix
    # topology[i][j] = 1 means i communicates with j
    for id in range(1, len(node_list)+1):
        if "remotes" in node_list[id]:
            for remote in node_list[id]["remotes"]:
                topology[node_map[remote]-1][id-1] = 1
        if "sensors" in node_list[id]:
            for sensor in node_list[id]["sensors"]:
                topology[id-1][node_map[sensor["endpoint"]]-1] = 1
    nodes_from = []
    nodes_to = []

    # Creation of the data structure expected for display
    for i in range(len(topology)):
        for j in range(len(topology[i])):
            if topology[i][j] == 1:
                nodes_from.append(i+1)
                nodes_to.append(j+1)

    # Creation of the dataframe with the communications
    df = pd.DataFrame({ 'from':nodes_from, 'to':nodes_to})

    # Build your graph
    G=nx.from_pandas_dataframe(df, 'from', 'to')

    # Plot it
    nx.draw(G, with_labels=True)
    plt.savefig(topo_name+".png")
    #plt.show()
