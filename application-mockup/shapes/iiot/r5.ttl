prefix san:   <http://www.irit.fr/recherches/MELODI/ontologies/SAN#>
prefix xsd:   <http://www.w3.org/2001/XMLSchema#>
prefix fn:    <http://w3id.org/sparql-generate/fn/>
prefix iter:  <http://w3id.org/sparql-generate/iter/>
prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
prefix adr:   <https://w3id.org/laas-iot/adream#>
prefix iotl:  <http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite#>
prefix ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
prefix ioto:  <http://www.irit.fr/recherches/MELODI/ontologies/IoT-O#>
prefix edr:   <http://w3id.org/laas-iot/edr#>
prefix ex:    <http://example.com/ns#>
prefix sh:    <http://www.w3.org/ns/shacl#>
prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix lmu:   <http://w3id.org/laas-iot/lmu#>
prefix dul:   <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
prefix time:  <http://www.w3.org/2006/time#>

ex:IIoTR5Envelope
	edr:hasTransferShape ex:IIoTR5TransferShape ;
	edr:hasApplyShape ex:IIoTR5ApplicableShape ;
	edr:hasDeliveryShape ex:IIoTR5ResultTransferShape ;
	edr:hasDeductionShape ex:IIoTR5ActiveShape.

ex:IIoTR5TransferShape
	a sh:NodeShape ;
	a edr:TransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this
				WHERE {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:producesDataOn adr:Temperature, adr:MachineState ;
							 	lmu:hasUpstreamNode [
									a lmu:HostNode;
								].
								FILTER NOT EXISTS {
									{ex:IIoTR5CoreRule edr:transferredTo $this.}
									UNION
									{ex:IIoTR5CoreRule edr:transferableTo $this.}
								}
						}
        }
        """ ;
  ].

ex:IIoTR5RuleTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:IIoTR5TransferShape ;
		sh:construct """
			PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
			PREFIX ex:<http://example.com/ns#>
			PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				ex:IIoTR5CoreRule edr:transferableTo $this.
				ex:IIoTR5CoreRule edr:transferredFrom ?host.
			} WHERE {
				$this lmu:hasUpstreamNode ?host.
				?host a lmu:HostNode.
			}
		""";
	].

ex:IIoTR5ApplicableShape
	a sh:NodeShape ;
	a edr:ApplicableShape ;
	a edr:NodeSensitiveComponent;
		sh:targetClass lmu:HostNode ;
	  sh:sparql [
	    sh:select """
	        PREFIX edr: <http://w3id.org/laas-iot/edr#>
	        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
	        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
	        prefix adr:   <https://w3id.org/laas-iot/adream#>
					prefix ex:    <http://example.com/ns#>
					PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
	        SELECT $this
					WHERE {
						FILTER NOT EXISTS {
							ex:IIoTR5CoreRule edr:isRuleActivable "true"^^xsd:boolean.
						}
						FILTER NOT EXISTS {
							$this a lmu:HostNode.
							$this lmu:hasDownstreamNode ?particleProvider, ?stateProvider.
							?temperatureProvider edr:producesDataOn adr:Temperature.
							?stateProvider	edr:producesDataOn adr:MachineState.
							FILTER NOT EXISTS {
								ex:IIoTR5CoreRule edr:isRuleActive "true"^^xsd:boolean.
							}
							FILTER EXISTS {
								$this lmu:hasDownstreamNode ?lowerNode.
								FILTER(?lowerNode = ?temperatureProvider || ?lowerNode = ?stateProvider)
								FILTER NOT EXISTS {
									?lowerNode edr:producesDataOn adr:Temperature, adr:MachineState.
								}
							}
						}
					}
	        """ ;
	  ].

ex:IIoTR5ApplicantRule
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:IIoTR5ApplicableShape ;
		sh:construct """
			prefix adr:   <https://w3id.org/laas-iot/adream#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			prefix ex:    <http://example.com/ns#>
			prefix lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:isInterestedIn adr:Temperature, adr:MachineState.
				$this edr:producesDataOn ex:ColdChainBroken.
				?partialDataProvider edr:partialDataProvider ?partialProduction.
				ex:IIoTR5CoreRule edr:isRuleActive "true"^^xsd:boolean.
			} WHERE {
				$this a lmu:HostNode.
				{
					$this lmu:hasDownstreamNode ?partialDataProvider.
					?partialDataProvider edr:producesDataOn ?partialProduction.
					FILTER NOT EXISTS {
						?partialDataProvider edr:producesDataOn adr:Temperature, adr:MachineState.
					}
				} UNION {
					ex:IIoTR5CoreRule edr:isRuleActivable "true"^^xsd:boolean.
				}
				OPTIONAL{ex:IIoTR5CoreRule edr:isRuleActive "false"^^xsd:boolean.}
			}
		""";
	].

ex:IIoTR5ActiveShape
		a sh:NodeShape ;
		sh:targetClass lmu:HostNode ;
		a edr:ActiveShape ;
	  sh:sparql [
	    sh:select """
	        PREFIX edr: <http://w3id.org/laas-iot/edr#>
	        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
	        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
	        prefix adr:   <https://w3id.org/laas-iot/adream#>
					prefix ex:    <http://example.com/ns#>
					PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
	        SELECT $this
					WHERE {
						FILTER NOT EXISTS {
							$this a lmu:HostNode.
							ex:IIoTR5CoreRule edr:isRuleActive "true"^^xsd:boolean.
						}
	        }
	        """ ;
	  ].

ex:IIoTR5ResultTransferShape
	a sh:NodeShape ;
	a edr:ResultTransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this
				WHERE {
						FILTER NOT EXISTS {
							{
								$this a lmu:Node ;
									edr:isInterestedIn ex:ColdChainBroken ;
								 	lmu:hasDownstreamNode [
										a lmu:HostNode;
									].
							} UNION {
								ex:IIoTR5CoreRule edr:ruleOriginatedFrom $this.
							}
							FILTER NOT EXISTS {
								{$this edr:consumesResult ex:IIoTR5CoreRule.}
							}
						}
        }
        """ ;
  ].

ex:IIoTR5ResultTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:IIoTR5ResultTransferShape ;
		sh:construct """
			PREFIX ex:<http://example.com/ns#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:consumesResult ex:IIoTR5CoreRule.
			} WHERE {
				$this a lmu:Node ;
			}
		""";
	].

ex:myIIoTApp a lmu:Application, lmu:Node ;
	iotl:exposes [
		iotl:endpoint "http://localhost:8006";
	].

ex:IIoTR5RuleShape
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule ex:IIoTR5CoreRule .

ex:IIoTR5CoreRule
	a sh:SPARQLRule ;
	sh:condition ex:IIoTR5ActiveShape ;
	edr:ruleOriginatedFrom ex:myIIoTApp ;
	edr:originatingEndpoint "http://localhost:8006" ;
	sh:construct """
		PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
		PREFIX ex:<http://example.com/ns#>
		PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
		PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
		PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
		PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
		PREFIX edr: <http://w3id.org/laas-iot/edr#>
		PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
		PREFIX iiot: <https://w3id.org/laas-iot/edr/iiot/factory#>
		CONSTRUCT {
			?deduction a rdf:Statement;
				rdf:subject ?machine;
				rdf:predicate rdf:type;
				rdf:object ex:ColdChainBroken;
				edr:deducedAt ?now;
				edr:deducedBy $this;
				edr:deducedWith ex:IIoTR5CoreRule;
				edr:deducedFrom ?temperature_obs, ?state_obs.
		} WHERE {
			?temperature rdf:type/rdfs:subClassOf* <https://w3id.org/laas-iot/adream#Temperature>;
				ssn:isPropertyOf ?zone.
			?temperature_obs ssn:observationResult/ssn:hasValue/dul:hasDataValue ?temperatureValue;
				ssn:observedProperty ?temperature;
				ssn:observedBy ?temperature_sensor.
			FILTER(?temperatureValue > '8.0'^^xsd:float)

			?state rdf:type/rdfs:subClassOf* <https://w3id.org/laas-iot/adream#MachineState>;
				ssn:isPropertyOf ?machine.
			?state_obs	ssn:observationResult/ssn:hasValue/dul:hasDataValue ?stateValue;
				ssn:observedProperty ?state;
				ssn:observedBy ?state_sensor.
			FILTER(?stateValue = '1.0'^^xsd:float)

			?machine a iiot:ColdSensitiveMachine;
				iiot:onConveyor?/iiot:locatedIn? ?zone.

			$this a lmu:HostNode.

			FILTER NOT EXISTS {
				?temperature_obs edr:usedForDeductionBy ex:IIoTR5CoreRule.
				?state_obs edr:usedForDeductionBy ex:IIoTR5CoreRule.
			}

			FILTER NOT EXISTS {
				?otherDeduction edr:deducedWith ex:IIoTR5CoreRule;
					edr:deducedFrom ?temperature_obs, ?state_obs.
			}
			BIND(URI(CONCAT('http://example.com/ns#comfortableSpotdeduction', STRUUID())) AS ?deduction )
			BIND(NOW() AS ?now)
		}
	""".
ex:ColdChainBroken rdfs:subClassOf <http://purl.oclc.org/NET/ssnx/ssn#Property>.
