package fr.irit.melodi.app.configuration;

import java.util.Set;

public class Configuration {
	// Sleep time between two observations
	private String name;
	private String uri;
	private Integer port;
	private String endpoint;
	private Set<String> ruleset;
	private boolean coapmode = false;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public Set<String> getRuleset() {
		return ruleset;
	}
	
	public boolean getCoapmode(){
		return this.coapmode;
	}

	public void setCoapmode(boolean coapmode){
		this.coapmode = coapmode;
	}

//	tu en es là, il faut que les applications envoient leurs règles une par une et non plus d'un bloc
	
	public void setRuleset(Set<String> ruleset) {
		this.ruleset = ruleset;
	}
}
