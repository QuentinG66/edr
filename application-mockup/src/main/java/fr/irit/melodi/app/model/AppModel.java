package fr.irit.melodi.app.model;

import java.util.ArrayList;
import java.util.List;

public class AppModel {
	private static AppModel instance;
	private List<String> listeningEndpoints;
	
	private AppModel(){
		this.listeningEndpoints = new ArrayList<>();
	}
	
	public static AppModel getInstance(){
		if(AppModel.instance == null){
			AppModel.instance = new AppModel();
		}
		return AppModel.instance;
	}
	
	public void addListener(String listeningEndpoint){
		this.listeningEndpoints.add(listeningEndpoint);
	}
	
	public List<String> getListeners(){
		return this.listeningEndpoints;
	}
	
	
}
