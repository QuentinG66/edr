package fr.irit.melodi.app.in.server;

import org.eclipse.californium.core.CoapServer;

import fr.irit.melodi.app.in.resources.CoapEnrichedEndpoint;
import fr.irit.melodi.app.in.resources.CoapNotifEndpoint;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.californium.core.CoapResource;

public class COAPServer extends Thread {

    private static final Logger LOGGER = LogManager.getLogger(COAPServer.class);

    private int port;
    private String url;
    private CoapServer server;

    public COAPServer(String url, int port) {
        this.port = port;
        this.url = url +":"+port;

    }
    
    public int getListeningPort(){
        return this.port;
    }

    public String getURL(){
        return this.url;
    }

    public void run() {
        LOGGER.info("[COAP] Setting server Coap for application" );
        server = new CoapServer(port);
        
        LOGGER.trace("[COAP] Server Coap for the application created on port : " + port);
        server.add(new CoapNotifEndpoint("notify"),
                   new CoapResource("data").add(new CoapEnrichedEndpoint("enriched")));
                                                        
        LOGGER.trace("[COAP] Ressources configured on application server Coap");
    //    server.addEndpoint(new CoapEndpoint(port)); */
        server.start();
        LOGGER.info("[COAP] Server Coap for the application started ! port :  " + port);
        
    }

}