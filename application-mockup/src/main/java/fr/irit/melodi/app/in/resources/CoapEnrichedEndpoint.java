package fr.irit.melodi.app.in.resources;

// import org.apache.logging.log4j.LogManager;
// import org.apache.logging.log4j.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;

import fr.irit.melodi.app.Controller;


public class CoapEnrichedEndpoint extends CoapResource {

   // private static final Logger LOGGER = LogManager.getLogger(CoapEnrichedEndpoint.class);

    public CoapEnrichedEndpoint(String name) {
		super(name);
	}

	@Override
	public void handlePOST(CoapExchange exchange) {

        exchange.accept();
        // 172 -> turtle format (choosen arbitrarily)
        if (exchange.getRequestOptions().isContentFormat(172)){
            String data = exchange.getRequestText();
            Controller.log(data);
        }
		exchange.respond(CREATED);
	}

}
