package fr.irit.melodi.app.in.resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;



public class CoapNotifEndpoint extends CoapResource {

    private static final Logger LOGGER = LogManager.getLogger(CoapNotifEndpoint.class);

    public CoapNotifEndpoint(String name) {
		super(name);
	}

	@Override
	public void handlePOST(CoapExchange exchange) {

        exchange.accept();
        // Possible mistake with the verificatio of type
        LOGGER.debug("[COAP] Application Notify" );
    
        LOGGER.info(exchange.getSourcePort());
        // equivalent to HTTP_NO_CONTENT (204)
		exchange.respond(CHANGED);
	}

}