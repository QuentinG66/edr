package fr.irit.melodi.app.out.resource;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.glassfish.jersey.client.ClientConfig;


public class Notifier {
	private static final Logger LOGGER = LogManager.getLogger(Notifier.class);

	public static Response notifyObservers(String endpoint, String observation){
		Client client = ClientBuilder.newClient(new ClientConfig());
		LOGGER.trace("Notifying observer "+endpoint);
		return client.target(endpoint)
	        .request(MediaType.TEXT_PLAIN)
	        .post(Entity.entity(observation, "text/csv"));
	}
	public static CoapResponse notifyObserversCoap(String endpoint, String observation) {
		String noHttpUri = endpoint.substring(4,endpoint.length());
		LOGGER.trace("[COAP] Notifying observer application"+ "coap"+ noHttpUri);
	    CoapClient client = new CoapClient("coap"+ noHttpUri);
		CoapResponse response = client.post(observation, MediaTypeRegistry.TEXT_CSV);
		return response;
	}

	public static Response subscribe(String endpoint, String rules){
		Client client = ClientBuilder.newClient(new ClientConfig());
		LOGGER.trace("Notifying observer "+endpoint);
		return client.target(endpoint)
	        .request(MediaType.TEXT_PLAIN)
	        .post(Entity.entity(rules, "text/turtle"));
	}

	public static CoapResponse subscribeCoap(String endpoint, String rule) {
		String noHttpUri = endpoint.substring(4,endpoint.length());
		LOGGER.trace("[COAP] Subscribe observer application "+ "coap"+ noHttpUri);
		CoapClient client = new CoapClient("coap"+ noHttpUri);
		// 172 correspond to turtle (not defined in MediaTypeRegistry)
		CoapResponse response = client.post(rule, 172);
		return response;
	}


}
