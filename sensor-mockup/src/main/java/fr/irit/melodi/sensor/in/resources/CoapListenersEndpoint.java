package fr.irit.melodi.sensor.in.resources;

import org.eclipse.californium.core.CoapResource;
//import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;

import fr.irit.melodi.sensor.model.Model;

import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;



public class CoapListenersEndpoint extends CoapResource {

    public CoapListenersEndpoint(String name) {
        super(name);
    }

   
    @Override
	public void handlePOST(CoapExchange exchange) {
		// accept the exchange and send an ack to the client
        exchange.accept();
       // if (exchange.getRequestOptions().isContentFormat(MediaTypeRegistry.TEXT_PLAIN)) {
       // Sourceport possible mistake ?
       String listeners = "[";
       for(String l : Model.getInstance(exchange.getSourcePort()).getListeners()){
           listeners += l+",";
       }
       listeners+="]";
        exchange.respond(CREATED,listeners);
        
    }
    /*@GET
	@Path("/listeners")
	public Response listListeners(@Context Request httpRequest){
		String listeners = "[";
		for(String l : Model.getInstance(httpRequest.getLocalPort()).getListeners()){
			listeners += l+",";
		}
		listeners+="]";
		return Response.status(HttpURLConnection.HTTP_OK)
		        .entity(listeners).build();
	}*/
}