package fr.irit.melodi.sensor.in.resources;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;

import fr.irit.melodi.sensor.model.Model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;



public class CoapListenerEndpoint extends CoapResource {

    private static final Logger LOGGER = LogManager.getLogger(CoapListenerEndpoint.class);

    public CoapListenerEndpoint(String name) {
        super(name);
    }

   
    @Override
	public void handlePOST(CoapExchange exchange) {
        // accept the exchange and send an ack to the client
        exchange.accept();
        if (exchange.getRequestOptions().isContentFormat(MediaTypeRegistry.TEXT_PLAIN)) {
            String listeningEndpoint = exchange.getRequestText().substring(4,exchange.getRequestText().length());
            String port_sensor = exchange.getRequestText().substring(0,4);
            LOGGER.debug("[COAP] ListenerEndpoint good format with source port :" + port_sensor);
            LOGGER.debug("[COAP] listening endpoint" + listeningEndpoint);
            // Sourceport possible mistake ?
            Model.getInstance(Integer.valueOf(port_sensor)).addListener(listeningEndpoint);
            exchange.respond(CREATED,"registred");
        }
        else {
            exchange.respond(UNSUPPORTED_CONTENT_FORMAT);
        }
	}

	/*@POST
	@Path("/listener")
	public Response register(String listeningEndpoint, @Context Request httpRequest){
		Model.getInstance(httpRequest.getLocalPort()).addListener(listeningEndpoint);
		return Response.status(HttpURLConnection.HTTP_OK)
        .entity("registered").build();
	}*/
}