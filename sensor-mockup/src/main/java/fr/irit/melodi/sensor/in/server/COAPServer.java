package fr.irit.melodi.sensor.in.server;

import org.eclipse.californium.core.CoapServer;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.californium.core.CoapResource;

import fr.irit.melodi.sensor.in.resources.CoapListenerEndpoint;
import fr.irit.melodi.sensor.in.resources.CoapListenersEndpoint;
import fr.irit.melodi.sensor.in.resources.CoapPingEndpoint;

public class COAPServer extends Thread {

    private static final Logger LOGGER = LogManager.getLogger(COAPServer.class);

    private int port;
    private String url;
    private CoapServer server;

    public COAPServer(String url, int port) {
        this.port = port;
//        this.baseUrl = url;
        this.url = url +":"+port;

    }
    
    public int getListeningPort(){
        return this.port;
    }

    public String getURL(){
        return this.url;
    }

    public void run() {
        LOGGER.info("[COAP] Setting server Coap for the sensors" );
        server = new CoapServer(port);
        
        LOGGER.trace("[COAP] Server Coap for the sensors created on port : " + port);
        server.add(new CoapResource("registration").add(new CoapPingEndpoint("ping"),
                                                        new CoapListenerEndpoint("listener"),
                                                        new CoapListenersEndpoint("listeners")));
        LOGGER.trace("[COAP] Ressources configured on server Coap");
    //    server.addEndpoint(new CoapEndpoint(port));
        server.start();
        LOGGER.info("[COAP] Server Coap for the sensors started ! port :  " + port);
        
    }

}