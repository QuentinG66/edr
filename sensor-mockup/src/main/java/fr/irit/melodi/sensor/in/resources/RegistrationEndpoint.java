package fr.irit.melodi.sensor.in.resources;

import java.net.HttpURLConnection;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.Request;

import fr.irit.melodi.sensor.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Path("/registration")
public class RegistrationEndpoint {

	private static final Logger LOGGER = LogManager.getLogger(RegistrationEndpoint.class);

	@GET
	@Path("/ping")
	public Response ping(@Context Request httpRequest){
		return Response.status(200).entity("pong "+httpRequest.getLocalPort()).build();
	}
	
	@POST
	@Path("/listener")
	public Response register(String listeningEndpoint, @Context Request httpRequest){
		LOGGER.trace("[HTTP] POST HANDLED : RegistrationListenerEndpoint with local port :" +httpRequest.getLocalPort());
		Model.getInstance(httpRequest.getLocalPort()).addListener(listeningEndpoint);
		return Response.status(HttpURLConnection.HTTP_OK)
        .entity("registered").build();
	}
	
	@GET
	@Path("/listeners")
	public Response listListeners(@Context Request httpRequest){
		String listeners = "[";
		for(String l : Model.getInstance(httpRequest.getLocalPort()).getListeners()){
			listeners += l+",";
		}
		listeners+="]";
		return Response.status(HttpURLConnection.HTTP_OK)
		        .entity(listeners).build();
	}
}
