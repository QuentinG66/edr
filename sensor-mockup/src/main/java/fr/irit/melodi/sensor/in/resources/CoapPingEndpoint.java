package fr.irit.melodi.sensor.in.resources;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;



public class CoapPingEndpoint extends CoapResource{

    public CoapPingEndpoint(String name) {
        super(name);
    }

    @Override
	public void handleGET(CoapExchange exchange) {
        //sourcePort wrong
		exchange.respond(VALID,"pong"+exchange.getSourcePort());
	}
	
}