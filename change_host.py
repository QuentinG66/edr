import os
import glob
import argparse
import json

def change_hosts(node, node_type, host):
    if "type" in node and node["type"]==node_type:
        node["host"]=host
    if "concentrates" in node:
        for child in node["concentrates"]:
            change_hosts(child, node_type, host)


parser = argparse.ArgumentParser(description='Formats adream sensor data to be replayed by an EDR simulation.')
parser.add_argument('-t', '--topology', help="Topology to update", type=str)
parser.add_argument('-n', '--nodetype', help="Type of nodes to update in the topology", type=str)
parser.add_argument('--host', help="New host for target nodes", type=str)

args = parser.parse_args()
with open(args.topology, "r") as topo_file:
    topology = json.load(topo_file)
    change_hosts(topology["layers"], args.nodetype, args.host)
with open(args.topology, "w") as topo_file:
    json.dump(topology, topo_file)
