import rdflib
import glob
import logging
import json

def extract_location(sensor):
    floor=None
    room=None
    associated_res = g.query(queries["get_associated"], initBindings={"device":rdflib.URIRef(sensor)})
    for associated_row in associated_res:
        floor_res = g.query(queries["get_floor"], initBindings={"device":rdflib.URIRef(associated_row[0])})
        for floor_row in floor_res:
            floor = floor_row["floor"]
        room_res = g.query(queries["get_room"], initBindings={"device":rdflib.URIRef(associated_row[0])})
        for room_row in room_res:
            room = room_row["room"]
    return str(floor), str(room)

def get_gallery_number(room):
    gallery = 0
    if room > 200:
        gallery = int((room - 200)/10)
    elif room > 100:
        gallery = int((room - 100)/10)
    else:
        gallery = int(room/10)
    return gallery


logging.basicConfig(level=logging.DEBUG)

sensor_types = {
    "http://elite.polito.it/ontologies/dogont.owl#TemperatureSensor":"temperature",
    "http://elite.polito.it/ontologies/dogont.owl#LightSensor":"luminosity",
    "https://w3id.org/laas-iot/adream#OutsideTemperatureSensor":"temperature",
    "http://elite.polito.it/ontologies/dogont.owl#PowerMeter":"power_consumption"}

logging.info("Loading queries...")
queries = {}
for q in glob.glob("queries/*.sparql"):
    qkey = q.split("/")[1].split(".")[0]
    with open(q, "r") as query_s:
        queries[qkey] = query_s.read()

logging.info("Loading the graph...")
g = rdflib.Graph()
g.parse("https://syndream.laas.fr:8082/adream/opa-common.owl")
g.parse("https://syndream.laas.fr:8082/adream-building/opa-adream.owl")

sensor_res = g.query(queries["list_sensors"])
sensors = []
floors = {}
rooms = {}

for sensor_row in sensor_res:
    sensor = {"url":str(sensor_row["sensor"]), "type":str(sensor_row["type"])}

    #id_res = g.query(queries["get_id"], initBindings={"device":rdflib.URIRef(sensor_row["sensor"])})
    #for id_row in id_res:
    #   print(id_row["id"])
    floor, room = extract_location(sensor_row["sensor"])
    if not str(None) in (floor, room):
        if not floor in floors:
            floors[floor] = set((room,))
        elif not room in rooms:
            floors[floor].add(room)
            rooms[room] = floor
        sensor["room"]=room
        sensor["floor"]=floor
        sensors.append(sensor)

building = []
for floor in floors:
    floor_topo = {"rooms":{}, "galleries":[]}
    galleries = {}
    # Sort the rooms in galleries, and store them in the floor dict
    for room in floors[floor]:
        room_fragment = room.split("#")[1]
        if room_fragment.startswith("H"):
            room_number = int(room_fragment[1:])
            gallery = get_gallery_number(room_number)
            if not gallery in galleries:
                galleries[gallery] = {room:[]}
            else:
                galleries[gallery][room]=[]
    # Remove the galleries with only one room
    for gallery in galleries:
        if len(galleries[gallery]) == 1:
            for room in galleries[gallery]:
                floor_topo["rooms"][room] = []
        else:
            floor_topo["galleries"].append(galleries[gallery])
    building.append(floor_topo)

# Insert the sensors
for sensor in sensors:
    for floor in building:
        for room in floor["rooms"]:
            if sensor["room"]==room:
                floor["rooms"][room].append(sensor)
        for gallery in floor["galleries"]:
            for room in gallery:
                if sensor["room"]==room:
                    gallery[room].append(sensor)
#print(building)
topology = {"name":"adream", "centralized":False, "layers":{}}
building_topo = {"type":"building", "count":1, "concentrates":[]}
topology["layers"]=building_topo
for floor in building:
    floor_topo = {"type":"floor", "count":1, "concentrates":[]}
    building_topo["concentrates"].append(floor_topo)
    for room in floor["rooms"]:
        room_topo = {"type":"room", "count":1, "concentrates":[]}
        floor_topo["concentrates"].append(room_topo)
        for sensor in floor["rooms"][room]:
            sensor_topo = {"type":"sensor", "measures":sensor_types[sensor["type"]], "count":1, "url":sensor["url"]}
            room_topo["concentrates"].append(sensor_topo)
    for gallery in floor["galleries"]:
        gallery_topo={"type":"gallery", "count":1, "concentrates":[]}
        floor_topo["concentrates"].append(gallery_topo)
        for room in gallery:
            room_topo = {"type":"room", "count":1, "concentrates":[]}
            gallery_topo["concentrates"].append(room_topo)
            for sensor in gallery[room]:
                sensor_topo = {"type":"sensor", "measures":sensor_types[sensor["type"]], "count":1, "url":sensor["url"]}
                room_topo["concentrates"].append(sensor_topo)

# Build the topology configuration
with open("adream.json", "w") as target:
    target.write(json.dumps(topology))
