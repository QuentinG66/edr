import sys
import pickle
import matplotlib.pyplot as plt
import glob

data = []
labels = []
fig = plt.figure()
ax  = fig.add_subplot(111)

results = [
"/home/nseydoux/dev/edr/results_sb_d1",
# "/home/nseydoux/dev/edr/results_sb_d12",
"/home/nseydoux/dev/edr/results_sb_d2",
# "/home/nseydoux/dev/edr/results_sb_d21",
# "/home/nseydoux/dev/edr/results_sb_d22",
"/home/nseydoux/dev/edr/results_sb_d3",
"/home/nseydoux/dev/edr/results_sb_d4"
]

for path in results:
    # centralized = []
    # for data_file in glob.glob(path+"/ded_delays_0_*"):
    #     # print(data_file)
    #     with open(data_file, "rb") as f:
    #         centralized.append(pickle.load(f))
    # data.append([x for l in centralized for x in l])
    # lbl = path.split("/")[5]
    # lbl = lbl.split("_")[2]
    # labels.append(lbl)

    decentralized = []
    for data_file in glob.glob(path+"/ded_delays_1_*"):
        # print(data_file)
        with open(data_file, "rb") as f:
            decentralized.append(pickle.load(f))
    data.append([x for l in decentralized for x in l])
    lbl = path.split("/")[5]
    lbl = lbl.split("_")[2]
    labels.append(lbl)

# plt.boxplot(data,0, '')

# with open("/home/nseydoux/dev/edr/results_rpi23sb_1/ded_delays_0_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S1, centralized")
#
# with open("/home/nseydoux/dev/edr/results_rpi23sb_1/ded_delays_1_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S1, distributed")
#
# with open("/home/nseydoux/dev/edr/results_rpi23sb_2/ded_delays_0_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S2, centralized")
#
# with open("/home/nseydoux/dev/edr/results_rpi23sb_2/ded_delays_1_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S2, distributed")
#
# with open("/home/nseydoux/dev/edr/results_rpi23sb_3/ded_delays_0_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S3, centralized")
#
# with open("/home/nseydoux/dev/edr/results_rpi23sb_3/ded_delays_1_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S3, distributed")
#
# with open("/home/nseydoux/dev/edr/results_rpi23sb_4/ded_delays_0_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S4, centralized")
#
# with open("/home/nseydoux/dev/edr/results_rpi23sb_4/ded_delays_1_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S4, distributed")
#
# with open("/home/nseydoux/dev/edr/results_rpi23sb_5/ded_delays_0_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S5, centralized")
#
# with open("/home/nseydoux/dev/edr/results_rpi23sb_5/ded_delays_1_officeLightComfortRule", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("S5, distributed")



# with open("/home/nseydoux/dev/edr/results_sb_d1/ded_delays_0_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D1, centralized")

# with open("/home/nseydoux/dev/edr/results_sb_d1/ded_delays_1_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D1, decentralized")

# with open("/home/nseydoux/dev/edr/results_sb_d11/ded_delays_0_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D11, centralized")

# with open("/home/nseydoux/dev/edr/results_sb_d11/ded_delays_1_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D11, decentralized")

# with open("/home/nseydoux/dev/edr/results_sb_d12/ded_delays_0_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D12, centralized")

# with open("/home/nseydoux/dev/edr/results_sb_d12/ded_delays_1_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D12, decentralized")

# with open("/home/nseydoux/dev/edr/results_sb_d2/ded_delays_0_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D2, centralized")

# with open("/home/nseydoux/dev/edr/results_sb_d2/ded_delays_1_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D2, decentralized")
#
# with open("/home/nseydoux/dev/edr/results_sb_d21/ded_delays_0_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D21, centralized")

# with open("/home/nseydoux/dev/edr/results_sb_d21/ded_delays_1_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D21, decentralized")
#
# with open("/home/nseydoux/dev/edr/results_sb_d22/ded_delays_1_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D22, decentralized")

# with open("/home/nseydoux/dev/edr/results_sb_d22/ded_delays_0_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D22, centralized")

# with open("/home/nseydoux/dev/edr/results_sb_d3/ded_delays_0_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D3, centralized")

# with open("/home/nseydoux/dev/edr/results_sb_d3/ded_delays_1_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D3, decentralized")

# with open("/home/nseydoux/dev/edr/results_sb_d4/ded_delays_0_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D4, centralized")
#
# with open("/home/nseydoux/dev/edr/results_sb_d4/ded_delays_1_R1", "rb") as f:
#     data.append(pickle.load(f))
#     labels.append("D4, decentralized")


# ax_1 = plt.subplot(122, sharex=ax_0, sharey=ax_0)
plt.boxplot(data,0, '')
ax.set_ylabel('Delay (s)')
# ax.set_xlabel('Topologies and strategies')
# ax.set_xticklabels(labels, rotation=30, fontsize=8)
ax.set_xticklabels(labels, fontsize=14)

plt.show()
