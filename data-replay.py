import csv
import json
import rdflib
from rdflib.namespace import Namespace
from rdflib.graph import Graph
import rdflib.plugins.sparql as sparql
import glob
import argparse

measure_types = {
    'http://purl.org/iot/vocab/m3-lite#BuildingTemperature':'temperature',
    'https://w3id.org/laas-iot/adream#Openness':'openness',
    'https://w3id.org/laas-iot/adream#ADREAMWindDirection':'wind_direction',
    'https://w3id.org/laas-iot/adream#ADREAMWindSpeed':"wind_speed",
    'https://w3id.org/laas-iot/adream#ElectricalPower':"power",
    'http://purl.org/iot/vocab/m3-lite#AirTemperature':"temperature",
    'https://w3id.org/laas-iot/adream#RotationSpeed':"rotation",
    'https://w3id.org/laas-iot/adream#ADREAMPluviometry':"pluviometry",
    'https://w3id.org/laas-iot/adream#ElectricalTension':"electrical_tension",
    'https://w3id.org/laas-iot/adream#ADREAMPyranometry':"pyranometry",
    'http://purl.org/iot/vocab/m3-lite#Temperature':"temperature",
    'https://w3id.org/laas-iot/adream#Luminosity':"luminosity",
    'https://w3id.org/laas-iot/adream#ADREAMHygrometry':"hygrometry",
    'https://w3id.org/laas-iot/adream#ADREAMAthmosphericPressure':"athmospheric_pressure",
    'https://w3id.org/laas-iot/adream#ElectricalIntensity':"electrical_intensity"
    }

SENSOR_HOST = "http://blacksad.laas.fr"
FLOOR_HOSTS = {"N0":"http://syndream.laas.fr", "N1":"http://rate.laas.fr", "N2":"http://thon.laas.fr", "N3":"http://syndream.laas.fr"}

def extract_sensors(data_file):
    sensors = set()
    with open(data_file, newline='') as observations:
        reader = csv.reader(observations)
        for observation in reader:
            sensors.add(observation[1])
    return sensors

def get_sensor_uri(graph, id):
    query ="""
    PREFIX ioto: <https://www.irit.fr/recherches/MELODI/ontologies/IoT-O#>
    SELECT ?s
    WHERE {
        ?s ioto:hasId "LABEL".
    }
    """.replace("LABEL", id)
    qres = graph.query(query)
    if len(qres)==0:
        return None
    for row in qres:
        return row["s"]

def get_types(graph, individual):
    types = []
    qres = graph.query("""
        SELECT ?t
        WHERE {
            <URI> rdf:type ?t.
        }
    """.replace("URI", individual))
    for row in qres:
        types.append(row["t"])
    return types

def get_floor(sensor_id):
    if "N0" in sensor_id or "RDC" in sensor_id:
        return "N0"
    elif "N1" in sensor_id:
        return "N1"
    elif "N2" in sensor_id:
        return "N2"
    else:
        return "N3"

def measure_traceable_data(data_file, sensor_caracs):
    described_sensors = get_sensors_with_uri(sensor_caracs)
    traceable = 0
    total = 0
    with open(data_file, newline='') as observations:
        reader = csv.reader(observations)
        for observation in observations:
            sensor_id = observation.split(",")[1]
            if sensor_id in described_sensors:
                traceable += 1
                if not sensor_id in sensor_caracs:
                    sensor_caracs[sensor_id] = {"obs_count":0}
                elif not "obs_count" in sensor_caracs[sensor_id]:
                    sensor_caracs[sensor_id]["obs_count"]=0
                sensor_caracs[sensor_id]["obs_count"] += 1
            total +=1
    return (traceable, total)

def get_sensors_with_uri(sensor_caracs):
    return [s for s in sensor_caracs if "uri" in sensor_caracs[s]]

def traceable_data_types(graph, sensor_caracs):
    types = set()
    for sensor in get_sensors_with_uri(sensor_caracs):
        qres = graph.query("""
            PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>
            SELECT ?t
            WHERE {
                <URI> ssn:observes ?t.
            }
        """.replace("URI", sensor_caracs[sensor]["uri"]))
        for row in qres:
            if "temperature" in row["t"].lower():
                types.add("http://purl.org/iot/vocab/m3-lite#Temperature")
                sensor_caracs[sensor]["obs_type"]="http://purl.org/iot/vocab/m3-lite#Temperature"
            else:
                sensor_caracs[sensor]["obs_type"]=row["t"]
                types.add(row["t"])
    return types

def retrieve_feature(graph, sensor_carac):
    q = sparql.prepareQuery("""
        PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>
        PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
        PREFIX opa: <https://w3id.org/laas-iot/adream#>
        SELECT ?room
        WHERE {
            ?sensor dul:AssociatedWith/opa:in_room ?room.
        }
    """)
    res = graph.query(q, initBindings={'sensor': rdflib.URIRef(sensor_carac["uri"])})
    if len(res) == 0:
        #print("No feature for sensor "+sensor_carac["id"])
        fragment = "_".join(sensor_carac["id"].split(".")[:3])
        return "https://w3id.org/laas-iot/adream-building#"+fragment
    else:
        for row in res:
            return str(row.room)

def store_sensor_data(sensor, source_path, target_path):
    with open(source_path, newline='') as source:
        with open(target_path, "a") as target:
            reader = csv.reader(source)
            for observation in reader:
                if observation[1] == sensor:
                    target.write(",".join(observation)+"\n")

def prepare_replay(sensors, data_folder, target_folder):
    for sensor in sensors:
        for source in glob.glob(data_folder+"/*.csv"):
            store_sensor_data(sensor, source, target_folder+"/"+sensor+".csv")

def extract_floors(sensor_caracs):
    floors = set()
    for s in sensor_caracs:
        if "floor" in sensor_caracs[s]:
            floors.add(sensor_caracs[s]["floor"])
    return floors

def generate_floor(floor_id, sensors):
    floor = {"type":"floor", "host":FLOOR_HOSTS[floor_id], "count":1}
    concentrates = []
    for s in [s for s in sensors if "floor" in sensors[s] and sensors[s]["floor"] == floor_id]:
        concentrates.append({"type":"sensor", "sensor_id":s, "measures":measure_types[str(sensors[s]["obs_type"])], "host":SENSOR_HOST, "count":1, "feature":sensors[s]["feature"]})
    floor["concentrates"] = concentrates
    return floor

def generate_building(sensors):
    building = {"type":"building", "concentrates":[], "host":"http://syndream.laas.fr", "count":1}
    floors = extract_floors(sensor_caracs)
    for f in floors:
        building["concentrates"].append(generate_floor(f, sensor_caracs))
    return building

def generate_topology(sensor_caracs):
    topology = {"name":"data-replay", "centralized":True}
    topology["layers"] = generate_building(sensors)
    return topology

parser = argparse.ArgumentParser(description='Formats adream sensor data to be replayed by an EDR simulation.')
parser.add_argument('-d', '--data', help="the folder containing the csv files to be replayed", type=str)
parser.add_argument('-o', '--output', help="The folder to which results are sent", type=str)
parser.add_argument('-r', '--rdf', help="The folder containing the building description files", type=str)

args = parser.parse_args()

description = Graph()
if not 'adream' in args.__dict__:
    print("Parsing the default KB")
    description.parse("https://w3id.org/laas-iot/adream/opa-common.owl")
    description.parse("https://syndream.laas.fr:8082/adream-building/opa-adream.owl")
else:
    for file in glob.glob(args.adream+"/*.owl"):
        description.parse(file, format="xml")

sensors = set()
for file in glob.glob(args.data+"/*.csv"):
    sensors = sensors | extract_sensors(file)

sensors_with_uri = set()
uris = set()
no_uris = set()
sensor_caracs = {}
# Sensors with URIs are collected
for sensor in sensors:
    uri = get_sensor_uri(description, sensor)
    if uri == None:
        no_uris.add(sensor)
        sensor_caracs[sensor] = {}
    else:
        uris.add(uri)
        sensors_with_uri.add(sensor)
        sensor_caracs[sensor] = {"uri":uri, "id":sensor}

# Data with a known type is measured, and each sensor is assigned its number of obs
traceable = 0
total = 0
for file in glob.glob(args.data+"/*.csv"):
    local_traceable, local_total = measure_traceable_data(file, sensor_caracs)
    traceable += local_traceable
    total += local_total
print(str(traceable)+"/"+str(total))

# Sensors are assigned their type of obs
data_types = traceable_data_types(description, sensor_caracs)

# Sensors are assigned to their floor
print("Extracting floors")
for s in get_sensors_with_uri(sensor_caracs):
    sensor_caracs[s]["floor"] = get_floor(s)
    if sensor_caracs[s]["floor"] == None:
        print(s)

# Sensors are assigned their feature of interest
print("Extracting features of interest")
for s in get_sensors_with_uri(sensor_caracs):
    sensor_caracs[s]["feature"]= retrieve_feature(description, sensor_caracs[s])

print("Organizing floors")
floors = {"N0":0, "N1":0, "N2":0, "N3":0}
for s in sensor_caracs:
    if "floor" in sensor_caracs[s]:
        floors[sensor_caracs[s]["floor"]]+=sensor_caracs[s]["obs_count"]

print("Generating topology")
topo = generate_topology(sensor_caracs)
with open(args.output+"/topology.json", "w") as topo_file:
    json.dump(topo, topo_file)

print("Organizing data to be replayed")
prepare_replay(sensors, args.data, args.output)
